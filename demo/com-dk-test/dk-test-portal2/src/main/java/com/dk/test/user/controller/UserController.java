package com.dk.test.user.controller;
import com.dk.test.entity.user.User;
import com.dk.test.user.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by duguk on 2018/1/9.
 */

@Controller
@RequestMapping("/v1/user")
@Api(value = "用户", description = "用户", produces = "application/json;charset=UTF-8")
public class UserController{
    @Autowired
    UserService userService;

    @ApiOperation(value = "用户查询", notes = "")
    @RequestMapping(value = "/",method = RequestMethod.GET)
    public @ResponseBody List<User> getChargeBill() {
//        List<User> users= new ArrayList<>();
//        User u= new User();
//        u.setName("simida");
//        users.add(u);
//        return users;
        return userService.selectListBySQL();
    }
}
