package com.dk.test.user.service;

import com.dk.foundation.engine.DataSource;
import com.dk.test.entity.user.User;
import com.dk.test.user.mapper.UserMapper;
import groovy.util.logging.Slf4j;
import io.shardingjdbc.core.api.HintManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by duguk on 2018/1/9.
 */

@Slf4j
@Service
public class UserService {

    @Autowired
    UserMapper userMapper;

    @DataSource(readRandom = true)
    public List<User> selectListBySQL()
    {
//        HintManager hintManager = HintManager.getInstance();
//        hintManager.setMasterRouteOnly();
         List<User> users = userMapper.selectListBySQL();

         List<User> users2=selectListBySQL2();

         return users2;
    }

    @DataSource(name="master")
    public List<User> selectListBySQL2()
    {
        return userMapper.selectListBySQL();
    }
}
